import { Schedule } from '@dealerfenix/schedules-core';
import { StringUtils } from '@dealerfenix/core';
let scheduleSetupDialog = {
    template:
    `<div class="modal-header">
         Schedule Setup
    </div>
    <div class="modal-body" id="modal-body">
        <table class="form">
            <tr>
                <td>Name</td><td>{{$ctrl.schedule.name}}</td>
            </tr>
            <tr>
                <td>Type</td>
                <td>
                    <select class="form-control" style="width:auto" ng-model="$ctrl.schedule.scheduleType">
                        <option ng-repeat="type in $ctrl.scheduleTypes" ng-value="type.value">{{type.name}}
                    </select>
                </td>
            </tr>
            
            <tr>
                <td>Aged Days</td><td><input class="form-control" ng-model="$ctrl.schedule.ageAlert"></td>
            </tr>

            <tr ng-if="$ctrl.canExpandAging()">
                <td><input type="checkbox" ng-model="$ctrl.setup.expandAging"> Expand Aging (> 30, > 60, etc.)</td>
            </tr>

            <tr ng-if="'RECEIVABLE_OTHER' != $ctrl.schedule.scheduleType">
                <td>Status List</td>
                <td>
                    <div class="btn-group">
                        <input class="form-control width12" ng-model="$ctrl.statusText">
                    </div>

                    <div class="btn-group">
                        <button class="btn btn-default" ng-disabled="!$ctrl.statusText" ng-click="$ctrl.onAddStatus()">Add</button>
                    </div>
                </td>
            </tr>

            <tr ng-if="'RECEIVABLE_OTHER' != $ctrl.schedule.scheduleType" ng-repeat="status in $ctrl.setup.statuses track by $index">
                <td></td>
                <td><a href ng-click="$ctrl.onDelete($index)"><i class="fas fa-times"></i></a> {{status}}</td> 
            </tr>
        </table> 
    </div>    
    <div class="modal-footer">
        <button class="btn btn-default" type="button" ng-click="$ctrl.onOk()">OK</button>
        <button class="btn btn-default" type="button" ng-click="$ctrl.onCancel()">Close</button>
    </div>`,
    controller:setupController
}

function setupController($uibModalInstance, scheduleService, $receivables, userService,  data) {

    let self = this;

    let fetchSetup = function() {
        $receivables.fetchSetup(data.scheduleId).then(setup=>{
            self.setup=setup;

        }).catch(err=>console.log("error fetching setup:" + err));
    }

    fetchSetup();

    scheduleService.fetchSchedule(data.scheduleId).then(schedule=>{
        self.schedule = schedule;
        self.scheduleTypes = Schedule.filterByReceivableScheduleType();
    }, error=>{
        console.log("ERROR fetching schedule " + JSON.stringify(error));
    })


    this.onDelete = function($index) {
        self.setup.statuses.splice($index, 1);

        self.setup.statuses.sort();
    }

    this.onAddStatus = function() {
        if (!Array.isArray(self.setup.statuses)) {
            self.setup.statuses = [];
        }

        self.setup.statuses.push(self.statusText);
        self.setup.statuses.sort();

        self.statusText = "";
    }

    this.canExpandAging=()=> {
        return self.schedule && StringUtils.isIn(self.schedule.scheduleType, ['RECEIVABLE_OTHER', 'RECEIVABLE_WARRANTY']);
    }
    
    this.onOk = function(schedule) {
        Promise.all([scheduleService.updateSchedule(self.schedule), $receivables.updateSetup(data.scheduleId, self.setup)]).then(()=>{
            $uibModalInstance.close({schedule:schedule})
        }, error=>{
            console.log("ERROR fetching schedule " + JSON.stringify(error));
            alert("There was an error updating the schedule");
        });
    }

    this.onCancel = function() {
        $uibModalInstance.dismiss('cancel');
    }
}

setupController.$inject = ["$uibModalInstance", "scheduleService",  "$receivables", "userService", "data"];

export default scheduleSetupDialog;