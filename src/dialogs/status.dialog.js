let statusDialog = {
    template:
    `<div class="modal-header">
        Update Status
    </div>
    <div class="modal-body" id="modal-body">
        <div style="min-height:300px;">
            <ul class="list-unstyled status-selector">
                <li ng-repeat="status in $ctrl.statuses"><a href ng-click="$ctrl.onUpdateStatus(status)">{{status}}</a></li>
            </ul>
        </div>
    </div>
    <div class="modal-footer">
        <div class="pull-left">
            <button class="btn btn-default" type="button" ng-click="$ctrl.onClear()">Clear</button>
        </div>
        <div class="pull-right">
            <button class="btn btn-default" type="button" ng-click="$ctrl.onCancel()">Close</button>
        </div>
    </div>`,
    controller:statusController
}

function statusController($uibModalInstance, scheduleService, userService,  data) {

    let self = this;

    this.statuses = data.statuses;

    this.onClear = function() {
        $uibModalInstance.close({status:null});
    }

    this.onUpdateStatus = function(status) {
        $uibModalInstance.close({status:status});
    }

    this.onCancel = function() {
        $uibModalInstance.dismiss('cancel');
    }

}

statusController.$inject = ["$uibModalInstance", "scheduleService", "userService", "data"];

export default statusDialog;