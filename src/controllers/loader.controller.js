
function loaderController($scope, $location, $routeParams) {

    $scope.onSuccess=function(){
        if ($routeParams.redirectUrl) {
            try {
                let path = atob($routeParams.redirectUrl);

                $location.search({});
                $location.path(path);
            } catch(e) {
                $location.search({});
                $location.path("/");
            }
        } else {
            $location.path("/choice");

        }
        $scope.$apply();
    }

}

loaderController.$inject = ["$scope", "$location", "$routeParams"];

export default loaderController;