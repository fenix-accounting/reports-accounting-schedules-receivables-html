import {ReportsMenuBuilder, DropDownOption} from '@dealerfenix/ngreports';
import {ChoiceDialog} from '@dealerfenix/schedules-core';
import scheduleSetupDialog from '../dialogs/schedule-setup.dialog';
import statusDialog from '../dialogs/status.dialog';
import { ReceivableUtil } from "../dto/recveiable-util";
import {RestError} from '@dealerfenix/fenixcore';
import {VehicleDialog} from '@dealerfenix/schedules-core';

function receivableController($scope, $rootScope, $location, scheduleService, windowService, dialogService, $sort, $printer, $receivables, userService, id){

    $rootScope.title = "Receivables";
    $scope.isExpanded = false;
    
    $scope.scheduleId = id;

    let ageAlert = 30;
    
    const searchDto = {
		statuses:[]
	}

    let fetchReceivables = function() {
        let setCompany = (locationId)=> {
            userService.getLocations().forEach(location=>{
                if (location.locationId == locationId) {
                    userService.setCurrentLocation(location);

                    window.location.reload();
                }
            });
        }



        let loadCompanyAndSchedules = (locationId, number)=> {
            console.log("loading schedule " + number + " in new company " + locationId);
          
            windowService.aSyncRun(scheduleService.fetchScheduleByNumber(locationId, number), result=>{
                $location.path(result.id);
            }, err=> {
                console.log("Error loading schedule " + err);
                setCompany(locationId);
            });
        }

        if (Array.isArray($scope.locationIds) && $scope.locationIds.length > 0) {
            windowService.aSyncRun(scheduleService.fetchReceivablesByNumber($scope.result.number, $scope.locationIds), result=>{

                if (!$sort.getOrderBy()) {
                    $sort.setOrderBy("age", false);
                }

                $scope.result = result;
                
                $rootScope.title = result.scheduleName;
    
                if ($scope.result.ageAlert && $scope.result.ageAlert > 0) {
                    ageAlert = $scope.result.ageAlert;
                }
    
                $sort.setObjects(result.receivableAccounts);
            
                windowService.sizeWidget("tblData");
    
            }, err=>console.log("error: " + err));

        } else {
            windowService.aSyncRun(scheduleService.fetchReceivables(id), result=>{
                if (result.locationId && result.locationId != userService.getCurrentLocation()) {
                    try {
                        loadCompanyAndSchedules(userService.getCurrentLocation(), result.number);
                    } catch(E) {
                        console.log("error " + E);
                    }
    
                } else {
                    if (!$sort.getOrderBy()) {
                        $sort.setOrderBy("age", false);
                    }
                    $scope.result = result;
                    
                    $rootScope.title = result.scheduleName;
        
                    if ($scope.result.ageAlert && $scope.result.ageAlert > 0) {
                        ageAlert = $scope.result.ageAlert;
                    }
        
                    $sort.setObjects(result.receivableAccounts);
                }
    
                windowService.sizeWidget("tblData");
    
            }, err=>console.log("error: " + err));
        }
       
    }

    $scope.onSort = (column)=> {
        $scope.result.receivableAccounts.sort( (a, b)=>{
            const i1 = a.currentBalance[column] ? a.currentBalance[column] : 0;
            const i2 = b.currentBalance[column] ? b.currentBalance[column] : 0;

            return ($sort.getSortAscending() ? i1 - i2 : i2-i1);
        });
    }

    $scope.sortByAccount = (account)=>{
        $scope.result.receivableAccounts.sort( (a, b)=>{
            const i1 = a.accountBalance[account] ? a.accountBalance[account] : 0;
            const i2 = b.accountBalance[account] ? b.accountBalance[account] : 0;

            return ($sort.getSortAscending() ? i1 - i2 : i2-i1);
        });
    }
    
    $scope.isAged = function(receivable) {
        if (!receivable)
            return false;
            
        return receivable.age > ageAlert;
    }

    $scope.onSwitchSchedule = function() {
        dialogService.openDialog({scheduleType:'RECEIVABLE'}, ChoiceDialog, result=>{


            if (result.schedule) {
                $location.path(ReceivableUtil.getSummaryPage(result.schedule) + result.scheduleId);
            } else {
                // use legacy pathing
                $location.path(result.scheduleId);
            }

        });
    }

    $scope.showReceivable = function(receivable) {
		if (!receivable)
			return true;

		if (searchDto.statuses && searchDto.statuses.length > 0) {
			let foundIt = false;

			for (let i=0; i < searchDto.statuses.length; i++) {
				if (searchDto.statuses[i] == receivable.status || (searchDto.statuses[i]  == '' && receivable.status == null) ) {
					foundIt = true;
					break;
				}
			}

			if (!foundIt)
				return false;
		
		}

        if ($scope.searchText) {
            let foundIt = false;

			let keys = ["controlNumber", "documentNumber",  "controlDescription", "lenderName"];

			for (let i=0; i < keys.length; i++) {
				const key = keys[i];
				if (receivable[key] && receivable[key].startsWith($scope.searchText))
				    return true;
			}

            return foundIt;
        }

		return true;
	}

    $scope.onDeal = function(deal) {
        $location.path("/deal/" + deal.documentId).search({source:id});
    }

    $scope.onVehicle = function(record) {
        if (record.vehicleId) {
            let vehicle = {vehicleId:record.vehicleId};
            dialogService.openDialog(vehicle, VehicleDialog);
        } else {
            alert("vehicle Id not found");
        }
    }

    $scope.onDetail = function(receivable) {
        $location.path("/detail/" + id + "/" + encodeURIComponent(receivable.controlNumber));
    }

    $scope.onComment = function() {
        fetchReceivables();
    }

    const key = "temp-receivable-setup." + id;

    let fetchSetup = function(force) {
        let _fetchSetup =()=>{
            return $receivables.fetchSetup(id);
        }

        $scope.statuses = [];

		userService.getFromLocalOrElse(key, _fetchSetup, 1000 * 60 * 5).then(receivableSetup=>{
            $scope.receivableSetup = receivableSetup;

            $scope.isExpanded = $scope.receivableSetup.expandAging;

            if (Array.isArray($scope.receivableSetup.statuses)) {
                $scope.doStatus = $scope.receivableSetup.statuses.length > 0;

                const statuses = $scope.receivableSetup.statuses;
                
                $scope.statuses.push(new DropDownOption("", "", true));
                for (let i=0; i < statuses.length; i++) {
                    $scope.statuses.push(new DropDownOption(statuses[i], statuses[i], true));	
                }
            }
        });
	}

    $scope.onUpdateStatus = function(receivable) {
        dialogService.openDialog({statuses:$scope.receivableSetup.statuses}, statusDialog, result=>{
            receivable.status = result.status;

			$receivables.updateReceivable(receivable).then(()=>{
				windowService.toast("Receivable Updated");
				// calcTotals();
			}).catch(err=>{
				console.log("error posting comment "+ err);
				dialogService.showError(RestError.parseErrorMessage(err));
			})
        });
    }
	
    $scope.onSearchControl = (control)=> {
        scheduleService.fetchReceivable(id, control).then(result=>{
            $scope.onDetail(result.receivableAccounts[0]);
        }, err=>{
            console.log("error fetching control" + err);
        })
        // alert(control);
    }

    $scope.onStatusFilter = function(selection) {
		
		let statuses = [];

		for (let i=0; i < selection.length; i++) {
			let item = selection[i];
			if (item.selected) {
				statuses.push(item.value)
			}
		}

		searchDto.statuses = statuses;
		calcTotals();
	}

    $scope.totalBalance = function(receivable) {
        if (!receivable)
            return 0;

        return receivable.current + receivable.overThirty + receivable.overSixty + receivable.overNinety + receivable.overOneHundredTwenty;
    }

    $scope.isMultiAccounts=() => {
        if (!$scope.result)
            return false;

        return Array.isArray($scope.result.accounts) && $scope.result.accounts.length > 1;
    }

    $scope.hasMultipleLocations = ()=> {
		return false;
	}

    $scope.onLocationChange = (locationIds)=> {
        $scope.locationIds = locationIds;
        fetchReceivables();
    }

    $scope.agedBalance = function(receivable) {
        if (!receivable)
            return 0;

        return receivable.overThirty + receivable.overSixty + receivable.overNinety + receivable.overOneHundredTwenty;
    }
    
    setTimeout(()=>{
        fetchReceivables();
        fetchSetup();

        checkisTablet();

        $('#tblData').on('scroll', function () {
            console.log("here");
            $('#tblHeader').scrollLeft($(this).scrollLeft());
        });
    },300);


    let doExport = function(format) {
        windowService.aSyncRun(scheduleService.exportReceivables(id, format), printFile=>{
            $printer.print(printFile);
        }, error=>{
            console.log("error: " + error)
            dialogService.showError("An error occurred while printing!");
        });
    }

    let onRefresh = function() {
        if (!$scope.refreshing) {
            $scope.refreshing = true;

            scheduleService.refreshSchedule([id]).then(()=>{
                windowService.toast("Schedule Refreshed");
                $scope.refreshing = false;

                fetchReceivables();
            }, err=>{
                console.log("error refreshing schedule "+ JSON.stringify(err));
                $scope.refreshing = false;
                dialogService.open({template:"ERROR", message:"Failed to refresh schedule"});
            })
        }
    }

    let onSetup = function() {
        dialogService.openDialog({scheduleId:id}, scheduleSetupDialog, ()=>{
            setTimeout(function(){
                userService.removeFromLocal(key);
                fetchSetup();

                fetchReceivables();
            }, 200);
        });
    }

    $scope.menuOptions = ReportsMenuBuilder(doExport);
    $scope.menuOptions.items.push({icon:"fas fa-sync fa-lg", link:onRefresh});

    if (userService.isAdmin()) {
        $scope.menuOptions.items.push({icon:"fas fa-cog fa-lg", link:onSetup});
    }

    let checkisTablet = function() {
        $scope.isTabletMode = windowService.windowSize().width < 1440;
	}

}

receivableController.$inject = ["$scope", "$rootScope", "$location", "scheduleService", "windowService", "dialogService", "$sort", "$printer", "$receivables", "userService", "id"];

export default receivableController;