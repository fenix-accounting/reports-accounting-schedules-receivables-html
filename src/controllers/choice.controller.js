import { ReceivableUtil } from "../dto/recveiable-util";

function choiceController($scope, $location, scheduleService, userService){

        let fetchSchedules = function() {
            scheduleService.fetchSchedules(userService.getCurrentLocation()).then(schedules=>{
                $scope.schedules=schedules;
            }).catch(err=>console.log("error fetching schedules:" + err));
        }

        fetchSchedules();

        $scope.isReceivable = function(schedule) {
            return schedule.scheduleType.startsWith("RECEIVABLE");
        }

        $scope.openSchedule = function(schedule) {
            $location.path(ReceivableUtil.getSummaryPage(schedule) + schedule.id);
        }
}

choiceController.$inject = ["$scope", "$location", "scheduleService", "userService"];

export default choiceController;