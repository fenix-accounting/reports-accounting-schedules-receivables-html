function dealController($scope, $location, salesRestService, id) {

    let source = $location.search().source;

    salesRestService.fetchDeal(id).then(function(deal) {
        $scope.deal = deal;
    }, function(error) {
        console.log(error);
    });

    $scope.onCancel = function() {
        $location.path("/deals/" + source);
    }
}

dealController.$inject=["$scope", "$location", "salesRestService", "id"];

export default dealController;