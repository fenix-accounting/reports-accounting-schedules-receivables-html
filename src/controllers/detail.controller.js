import {PatchDialog} from '@dealerfenix/schedules-core';
import { ReceivableUtil } from "../dto/recveiable-util";
import {CollectionUtils} from "@dealerfenix/core";
import {DLG_YES} from "@dealerfenix/ngwindows";
import angular from 'angular';

function detailController($scope, $location, scheduleService, $sort, userService, windowService, dialogService, $printer, id, control){

    $scope.control = {number:control, description:""};
    $scope.items   = [];
    $scope.showZeroBalance = false;

    $scope.toggleZeroBalance = ()=> {
        $scope.showZeroBalance = !$scope.showZeroBalance;
        updateStats();
    };

    $scope.total = 0;

    let updateStats = function() {
    
        $scope.total = 0;
        $scope.items = [];
        
        if ($scope.isControl2() && !$scope.showZeroBalance) {
            let controlMap = {};
            
            for (let i=0; i < $scope.detail.reportDetail.length; i++) {
                let record = $scope.detail.reportDetail[i];
            
                if (controlMap[record.applyTo]) {
                    let controlRecord = controlMap[record.applyTo];

                    let d1 = new Date(record.transactionDate);
                    let d2 = new Date(controlRecord.transactionDate);

                    if (d1 < d2) {
                        controlRecord.journalSourceCode = record.journalSourceCode;
                        controlRecord.transactionDate = record.transactionDate;
                        controlRecord.accountNumber = record.accountNumber;
                        controlRecord.referenceNumber = record.referenceNumber;
                    }

                    controlRecord.amount += record.amount;
                    
                } else {
                    controlMap[record.applyTo] = angular.copy(record);
                    $scope.items.push(controlMap[record.applyTo]);
                }
            }

            $scope.items.forEach(item=>{
                item.amount = item.amount.toFixed(2);
            })

            CollectionUtils.removeIf($scope.items, item=>{
                return Number(item.amount) === 0;
            });

        } else {
            $scope.items = $scope.detail.reportDetail;
        }


        for (let i=0; i < $scope.detail.reportDetail.length; i++) {
            let record = $scope.detail.reportDetail[i];
            
            $scope.total += record.amount;
        }

        $sort.setObjects($scope.items);

        $scope.control.description = $scope.detail.description;

        $scope.isTabletMode = windowService.windowSize().width < 1200;
        if (!$scope.isTabletMode) {
            windowService.sizeWidget("tblData");
        }
    
    }

    let fetchScheduleDetail = function(format) {

        let fetchDetail = ()=> {
            windowService.aSyncRun(scheduleService.fetchScheduleControlDetail(id, control, format), detail=>{
                $scope.detail=detail;
        
                updateStats();
            }, err=>{
                console.log("error :" + err);
            });
        }

        $scope.comments = [];

        windowService.aSyncRun(scheduleService.fetchReceivable(id, control), result=>{

            if (result.receivableAccounts.length == 1) {
                fetchComments(result.receivableAccounts[0].id);
            }

            fetchDetail();

        }, err=> {

            fetchDetail();
        });

    
    }

    $scope.onBack = function() {
        $location.path(ReceivableUtil.getSummaryPage($scope.detail) + id);
    }

    let onExport = function(format) {
        windowService.aSyncRun(scheduleService.fetchScheduleControlDetail(id, control, format), response=>{
            $printer.print(response);
        }, err=>{
            console.log("error :" + err);
        });
    }

    $scope.isControl2 = ()=> {
        return $scope.detail && $scope.detail.balanceMethod == "BALANCE_BY_ITEM";
    }
    
    fetchScheduleDetail();

    let fetchComments = function(id) {
        scheduleService.fetchContractComments(id).then(comments=>{
            $scope.comments = comments;
        }, error=>{
            dialogService.showError(error);
        })
    }


    $scope.menuOptions = {items:[
        {link:onExport, icon:"far fa-file-pdf fa-lg", data:"PDF"},
        {link:onExport, icon:"far fa-file-excel fa-lg", data:"XLS"},
        // {link:refreshData, icon:"fas fa-sync fa-lg"}
    ]};

    let calcOpenBalance = function() {
        if (!$scope.detail)
            return 0;

        let openBalance = $scope.detail.openingBalance;

        if (!openBalance)
            return 0;

        return openBalance.current + openBalance.overThirty + openBalance.overSixty + openBalance.overNinety + openBalance.overOneHundredTwenty;
    }

    if (userService.getPrimaryRole() == "ADMIN"){
        let onPatchBalance = function() {
            dialogService.openDialog({scheduleId:id, controlNumber:control}, PatchDialog, ()=>{
                fetchScheduleDetail();
            });
        }

        let clearEntry = ()=> {
            dialogService.open({template:"YES_NO", message:"Are you sure you want to clear this item from the schedule"}, result=>{

                if (DLG_YES == result) {
                    const balance = calcOpenBalance() - $scope.total;
                    scheduleService.patchBeginBalance(id, [{controlNumber:control, scheduleBalance:{current: balance.toFixed(2)}}]).then(()=>{
                        $scope.onBack();
                    }, err=>{
                        console.log("error clearing entry");
                    })
                }
    
            })
        }

        $scope.menuOptions.items.push({icon:"fas fa-ellipsis-v", children:[{label:"Patch", link:onPatchBalance}, {label:"Clear", link:clearEntry}]});
    }
}

detailController.$inject =["$scope", "$location", "scheduleService", "$sort", "userService", "windowService", "dialogService", "$printer", "id", "control"];

export default detailController;