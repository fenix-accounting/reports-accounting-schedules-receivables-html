export class ReceivableUtil {

    static getSummaryPage(schedule) {
        if (!schedule.scheduleType) {
            return "/";
        }

        switch (schedule.scheduleType) {
            case "RECEIVABLE_VEHICLE_INCENTIVES" : case "RECEIVABLE_FI" :
                return (schedule.enhancedView ? "/deals/" : "/");
            default :
                return "/";
        }
    }
}