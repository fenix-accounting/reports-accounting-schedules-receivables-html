function CommentServiceProvider(scheduleService, userService) {

    this.fetchComments = (id)=> {
        return scheduleService.fetchReceivableComments(id);
    }

    this.addComment = (id, comment, recipients)=> {
        let newComment = {objectId:id, body:comment, postedBy:userService.getUser().username, postedOn:new Date(), recipients:recipients};

		return scheduleService.postReceivableComment([newComment]);

    }
}


export default CommentServiceProvider;