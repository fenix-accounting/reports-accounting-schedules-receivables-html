function receivableRestService(restClient, $registry) {

    let getScheduleService = function() {
        return $registry.fetchService('accounting-schedules-api');
    }

    this.fetchSetup = function(scheduleId) {
        let citUrl = getScheduleService() + "/receivable/setup/" + scheduleId;

        return restClient.get(citUrl);
    }

    this.updateSetup = function(scheduleId, setup) {
        let url = getScheduleService() + "/receivable/setup/" + scheduleId;

        return restClient.post(url, setup);
    }


    // this.fetchContract = function(contractId) {
    //     let citUrl = getScheduleService() + "/contracts/contract/" + contractId;

    //     return restClient.get(citUrl);
    // }

    this.updateReceivable = function(receivable) {
        let url = getScheduleService() + "/receivable";

        return restClient.post(url, receivable);
    }

    // this.searchByContractsInTransitByControlNumber = function(locationId, controlNumber) {
    //     return restClient.get(getScheduleService() + "/contracts/" + locationId + "?controlNumber=" + controlNumber);
    // }

}

receivableRestService.$inject = ["restClient", "$registry"];

export default receivableRestService;