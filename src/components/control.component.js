let ControlComponent = {
    bindings: {
        control:'<'
    },
    template:
    `<div class="fx-wrapper card">
        <table class="control-info">
            <tr>
                <td class="caption">Control #</td>
                <td>{{$ctrl.control.number}}</td>
            </tr>
            <tr>
                <td class="caption">Desc</td>
                <td>{{$ctrl.control.description}}</td>
             </tr>
        </table>
    </div>`
}

export default ControlComponent;