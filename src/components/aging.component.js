let agingComponent = {
    bindings:{
        receivables:"<"
    },
    template:
    `
    <div class="btn-group">
        <table class="aging-summary">
            <tr>
                <th>Current</th><th>Ovr 30</th><th>Ovr 60</th><th>Ovr 90</th><th>120+</th>
            </tr>
            <tr>
                <td class="money">{{$ctrl.current | currency:undefind:0}}</td>
                <td class="money">{{$ctrl.over30 | currency:undefind:0}}</td>
                <td class="money">{{$ctrl.over60 | currency:undefind:0}}</td>
                <td class="money">{{$ctrl.over90 | currency:undefind:0}}</td>
                <td class="money">{{$ctrl.over120 | currency:undefind:0}}</td>
            </tr>
        </table>
    </div>`,
    controller:agingController
}

function agingController() {

    let self = this;

    let init = function() {
        self.current  = 0;
        self.over30   = 0;
        self.over60   = 0;
        self.over90   = 0;
        self.over120  = 0;
    }

    let calc = function() {
        if (self.receivables){
            self.receivables.forEach(receivable=>{
                if(receivable.age < 30) {
                    self.current += receivable.balance;
                } else if (receivable.age < 60) {
                    self.over30 += receivable.balance;
                } else if (receivable.age < 90) {
                    self.over60 += receivable.balance;
                } else if (receivable.age < 90) {
                    self.over90 += receivable.balance;
                } else {
                    self.over120 += receivable.balance;
                }
            });
        }
    }

    this.$onChanges = function(){
        init();
        calc();
    }

    this.$onInit = function(){
        init();
    }
}

export default agingComponent;