let KpiComponent = {
    bindings: {
        receivable:'<',
        ageAlert:'<'
    },
    template:
    `<div class="fx-wrapper recv-kpi-bar">
        <div class="btn-group kpi-bar-item">
            <kpi-stat color="'bg-grey'" value="$ctrl.totalBalance(result.scheduleBalance) | currency:undefined:0" text="'Total Balance'"></kpi-stat>
        </div>

        <div class="btn-group kpi-bar-item">
            <kpi-stat color="'bg-red'" value="$ctrl.agedBalance(result.scheduleBalance) | currency:undefined:0" text="' Past Due'"></kpi-stat>
        </div>
    </div>`,
    controller:kpiController
}

function kpiController() {
    const self = this;
    this.totalBalance = function() {
        if (!self.receivable)
            return 0;

        return self.receivable.current + self.receivable.overThirty + self.receivable.overSixty + self.receivable.overNinety + self.receivable.overOneHundredTwenty;
    }

    this.agedBalance = function() {
        if (!self.receivable)
            return 0;

        if (self.ageAlert > 0) {
            let total = 0;

   
            if (self.ageAlert >= 120) {
                return self.receivable.overOneHundredTwenty;

            } 
            
            if (self.ageAlert >= 90 ) {  
                return self.receivable.overNinety + self.receivable.overOneHundredTwenty;

            } 
            
            if (self.ageAlert >= 60 && self.ageAlert < 90) {
                return self.receivable.overSixty + self.receivable.overNinety + self.receivable.overOneHundredTwenty;

            } 
        }

        return self.receivable.overThirty + self.receivable.overSixty + self.receivable.overNinety + self.receivable.overOneHundredTwenty;
    }
}

kpiController.$inject = [];

export default KpiComponent;