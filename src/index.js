import 'angular';
import 'angular-route';
import 'ngstorage';
import 'jquery';
import 'bootstrap/dist/js/bootstrap.js';
import 'angular-ui-bootstrap';

import '@dealerfenix/fenixcore';
import '@dealerfenix/ngreports';

import '@dealerfenix/accounting-core';
import '@dealerfenix/schedules-core';
import '@dealerfenix/sales-core';
import choiceController from "./controllers/choice.controller";
import detailController from "./controllers/detail.controller";
import receivableController from "./controllers/receivable.controller";
import dealController from './controllers/deal.controller';
import loaderController from './controllers/loader.controller';

import routes from "./routes/app.routes";
import agingComponent from './components/aging.component';
import ControlComponent from './components/control.component';
import KpiComponent from './components/kpis.component';
import receivableRestService from './services/receivable.service';
import CommentServiceProvider from './services/comment.service';

import '../style/style.scss';

angular.module('receivablesApp', ['fenixCore', "ngReports", "ngWindows", 'schedulesCoreApp', 'salesCore', 'ngStorage', 'ngRoute','ui.bootstrap'])
    .config(["$routeProvider", "$httpProvider", routes])
    .controller('loaderController', loaderController)
    .controller('choiceController', choiceController)
    .controller('detailController', detailController)
    .controller('receivableController', receivableController)
	.controller('dealController', dealController)
	.service('$receivables', receivableRestService)
    .component('aging', agingComponent)
	.component("control", ControlComponent)
	.component('kpiBar', KpiComponent)
	.factory("commentProvider", ["scheduleService", "userService", (scheduleService, userService)=>{
		return new CommentServiceProvider(scheduleService, userService);
	}])
    .run(["scheduleInit", "$location", "$sessionStorage", function(scheduleInit, $location, $sessionStorage){
		scheduleInit.init(process.env.NODE_ENV === 'development')
		if (!$sessionStorage.user) {
			if ($location.path()!="" && $location.path() != "/") {
				let path = $location.absUrl().replace($location.path(), "");

				let argsIndex = path.indexOf("?");
				if ( argsIndex > -1) {
					path = path.substring(0, argsIndex);
				}

				window.location.href = path + "?redirectUrl=" + btoa($location.url());
			}
		}
	}]);  