function routes($routeProvider, $httpProvider) {
   
    $routeProvider


        .when("/", {
            templateUrl : './pages/loader.html',
            controller : 'loaderController'
        })
    
        .when("/choice", {
            templateUrl : './pages/choice.html',
            controller : 'choiceController'
        })
        
        .when("/:id", {
            templateUrl : './pages/receivable.html',
            controller : 'receivableController',
            resolve: {
                id : ["$route", function($route) {
                    return $route.current.params.id;
                }]
            }
        })

        .when("/deals/:id", {
            templateUrl : './pages/deals.html',
            controller : 'receivableController',
            resolve: {
                id : ["$route", function($route) {
                    return $route.current.params.id;
                }]
            }
        })


        .when("/deal/:id", {
            templateUrl : './pages/deal.html',
            controller : 'dealController',
            resolve: {
                id : ["$route", function($route) {
                    return $route.current.params.id;
                }]
            }
        })
     
        .when("/detail/:id/:control", {
            templateUrl : './pages/detail.html',
            controller : 'detailController',
            resolve: {
                id : ["$route", function($route) {
                    return $route.current.params.id;
                }],
                control : ["$route", function($route) {
                    return $route.current.params.control;
                }]
            }
        })
     
        .otherwise({
            redirectTo : '/'
        });
    
        $httpProvider.interceptors.push('httpUnauthorized');
}

export default routes;